#
# Cookbook:: telegraf
# Recipe:: install_windows
#
# Copyright:: 2018, Svyatoslav Dubovik, All Rights Reserved.
#

package_url = "https://dl.influxdata.com/telegraf/releases/telegraf-#{node['telegraf']['version']}_windows_amd64.zip"
package_file = ::File.join(Chef::Config[:file_cache_path], ::File.basename(package_url))

remote_file 'download_package' do
  path package_file
  source package_url
  notifies :run, 'execute[uninstall_telegraf]', :immediately
  not_if { ::File.exist?(package_file) }
end

execute 'uninstall_telegraf' do
  command "#{node['asapps']['directory']}/telegraf/telegraf.exe --service uninstall"
  action :nothing
  only_if { ::Win32::Service.exists?('telegraf') }
  notifies :stop, 'service[telegraf]', :before
end

directory node['asapps']['directory'] do
  recursive true
  action :create
end

windows_zipfile node['asapps']['directory'] do
  source package_file
  overwrite true
  action :unzip
  not_if { ::Win32::Service.exists?('telegraf') }
end

execute 'install_telegraf' do
  command "#{node['asapps']['directory']}/telegraf/telegraf.exe --service install --config #{node['asapps']['directory']}/telegraf/telegraf.conf"
  action :run
  not_if { ::Win32::Service.exists?('telegraf') }
end

service 'telegraf' do
  action [:enable, :start]
end
