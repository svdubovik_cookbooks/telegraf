#
# Cookbook:: telegraf
# Recipe:: default
#
# Copyright:: 2018, Svyatoslav Dubovik, All Rights Reserved.
#

if node['platform'] == 'windows'
  include_recipe 'telegraf::install_windows'
end
